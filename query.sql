-- Active: 1709647912497@@127.0.0.1@3306@ProjetSql


SELECT * FROM student ;

-- Lister les salles d'un centre
SELECT * FROM room WHERE id_center =3;

-- Modifier la capacité d'une room
UPDATE room set capacity = 18
WHERE name = "chambre une";

SELECT * FROM room;

-- Modifier ou supprime une réservation
UPDATE booking set `startDate`= "2022-04-12", `endDate`= "2023-06-12"
WHERE id_promo = 1;

SELECT * FROM booking;

DELETE FROM booking
WHERE id = 3;

SELECT * FROM booking;

--LISTER LES CENTRES D'UNE REGION
SELECT * FROM center WHERE id_region = 3;

--LISTER LES PROMO D'UN CENTRE
SELECT * FROM promo WHERE id_center = 1;

--AFFICHER LE PLANING COMPLET PAR SALLE
SELECT * FROM booking WHERE id_room = 2;

--AFFICHER LE PLANING COMPLET PAR PROMO
SELECT * FROM booking WHERE id_promo=2;

--MODIFIER LA LISTE DES APPRENANTS
UPDATE student SET name="JeanMich" WHERE name="POPOL";

--VERIFIER LE CHANGEMENT
SELECT * FROM student;