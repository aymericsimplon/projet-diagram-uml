-- Active: 1709647912497@@127.0.0.1@3306@ProjetSql


DROP TABLE IF EXISTS promo_student;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS center;
DROP TABLE IF EXISTS region;

CREATE TABLE region (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64)
);

CREATE TABLE center (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(65),
    id_region INT,
    Foreign Key (id_region) REFERENCES region(id)
);

CREATE TABLE promo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (64),
    startDate DATE,
    endDate DATE,
    studentNumber INT,
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id)
);
CREATE TABLE room (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(68),
    capacity INT,
    color VARCHAR(69),
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id)
);

CREATE TABLE booking (
    id INT PRIMARY KEY AUTO_INCREMENT,
    startDate DATE,
    endDate DATE,
    id_room INT,
    id_promo INT,
    Foreign Key (id_room) REFERENCES room(id),
    Foreign Key (id_promo) REFERENCES promo(id)
);
CREATE TABLE student (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64)
);

CREATE TABLE promo_student(
    id_promo INT,
    id_student INT,
    PRIMARY KEY (id_promo,id_student),
    Foreign Key (id_promo) REFERENCES promo (id) ON DELETE CASCADE,
    Foreign Key (id_student) REFERENCES student(id) ON DELETE CASCADE
);

INSERT INTO region (name) VALUES
("Rhône-Alpes"),
("Acquitaine"),
("Paname");

INSERT INTO center (name,id_region) VALUES
("Simplon",1),
("Machin",2),
("Blabla",3);

INSERT INTO promo ( name, startDate, endDate, studentNumber,id_center) VALUES
("Dév web", '2023-01-08', '2024-01-10', 27,1),
("Dév ia", '2023-03-08', '2024-03-11', 26,1),
("Technicien Assistance Informatique", '2023-01-10', '2024-01-12', 23,2);

INSERT INTO room (name,capacity,color, id_center) VALUES
("chambre une", 2,"bleu", 1),
("chambre deux", 1,"violet", 3),
("chambre trois", 4,"rouge", 3);

INSERT INTO booking (startDate, endDate,id_promo, id_room) VALUES
('2022-04-10', '2023-05-12',1,2),
('2022-05-01', '2023-06-15',2,2);

INSERT INTO student (name) VALUES
("JEANPOL"),
("POPOL"),
("MOHA");






